import yaml
import
import asyncio
import pytesseract
import aiohttp
import keyboard
from multiprocessing import Process
from PIL import ImageOps, ImageFilter, ImageGrab

config = yaml.load(open("config/config.yaml", "r"))
ranks = {
    "0": "Unranked",
    "1": "Copper IV",
    "2": "Copper III",
    "3": "Copper II",
    "4": "Copper I",
    "5": "Bronze IV",
    "6": "Bronze III",
    "7": "Bronze II",
    "8": "Bronze I",
    "9": "Silver IV",
    "10": "Silver III",
    "11": "Silver II",
    "12": "Silver I",
    "13": "Gold IV",
    "14": "Gold III",
    "15": "Gold II",
    "16": "Gold I",
    "17": "Plat III",
    "18": "Plat II",
    "19": "Plat I",
    "20": "Diamond",
}


def start_getting_player(name):
    try:
        asyncio.get_event_loop().run_until_complete(get_player(name))
    except RuntimeError:
        print("The last action is not done yet! Wait a second!")


async def get_player(name):
    name = name.replace("—", "-")
    async with aiohttp.ClientSession() as session:
        async with session.get(f"https://r6tab.com/api/search.php?platform=uplay&search={name}") as resp:
            text = await resp.json()
            if text is None:
                print("-" * 30)
                print("No Player found!")
            elif text["totalresults"] == 0:
                print("-" * 30)
                print(f"Player: {name} not found!")
            else:
                text = text["results"][0]
                print("-" * 30)
                print(f"Player:\n  Name: {text['p_name']}\n  Rank: {ranks[text['p_currentrank']]}")


async def capture_image():
    print("Taking Screenshot!")
    original = ImageGrab.grab()
    if config["debug"]:
        original.save("files/SCREENSHOT.jpg")
    print("Looking up Players from Screenshot!")

    cropped_image = original.crop((config["top_left"][0], config["top_left"][1], config["bottom_right"][0], config["bottom_right"][1]))

    cropped_image = ImageOps.invert(cropped_image)

    if config["debug"]:
        cropped_image.save("files/CROPPED.jpg")

    heigth = cropped_image.size[1] / 5

    players_left = 0

    while players_left <= 4:
        top = heigth * players_left
        bottom = heigth * (players_left + 1)

        new_crop = cropped_image.crop((0, top, cropped_image.size[0], bottom))

        new_crop = new_crop.filter(ImageFilter.SHARPEN)

        if config["debug"]:
            new_crop.save(f"files/CROPPED_{players_left}.jpg")
        read_as = pytesseract.image_to_string(new_crop)
        Process(target=start_getting_player, args=(read_as,)).start()
        players_left += 1
    print("Startet all of them!")
    loop()


def loop():
    while True:
        if keyboard.is_pressed(config["input_modifier"]) and keyboard.is_pressed(config["input_one"]):
            asyncio.get_event_loop().run_until_complete(capture_image())
            break
        elif keyboard.is_pressed(config["input_modifier"]) and keyboard.is_pressed(config["input_two"]):
            print("-"*30)
            manual_input = input("Type Name: ")
            print("Looking up: " + manual_input)
            start_getting_player(manual_input)


if __name__ == "__main__":
    print("-" * 40)
    print("PROGRAM STARTED!\nRainbow six Enemy lookup V1.0, by CCHD")
    print("-"*40)
    print(f"Use \"{config['input_modifier']} + {config['input_one']}\" To take a screenshot and look up the enemys in it")
    print("(It's best to do this while picking operators, since then the background is dark and the names are all white)")
    print(f"Use \"{config['input_modifier']} + {config['input_two']}\" To enable custom search in this console, just type the name and hit enter!")
    print("-"*40)
    print("")
    loop()
