# Rainbow Six: Siege enemy lookup

## What this script does
This script takes a screenshot of the tab-list in-game and then uses OCR to detect the enemys names.
It then runs the names through an API, getting the players Names and Ranks, so you know who you're up against!

## How To install
+ Download this project and unzip it somewhere safe
+ Install [Tesseract](https://digi.bib.uni-mannheim.de/tesseract/tesseract-ocr-setup-3.05.02-20180621.exe) (Thanks Uni-Mannheim!)
+ Install [Python 3.6](https://www.python.org/ftp/python/3.6.7/python-3.6.7-amd64.exe)
+ Open a cmd (as admin preferably)
+ Enter `cd /path/to/the/repository`
+ Enter `python -m pip install -r requirements.txt`
+ Edit the [configs](config/config_example.yaml) to your liking
+ Run the [.bat script](run.bat) (You can also link to the desktop, by right clicking the script and hitting send to -> Desktop)

## How to configure
+ Head into rainbow six and go into a game (casual or ranked doesn't matter)
+ Take a screenshot to measure (You could use [ShareX](https://github.com/ShareX/ShareX/releases/download/v12.4.1/ShareX-12.4.1-setup.exe) for that)
+ Insert that screenshot into a program like MS-Paint or [GIMP](https://download.gimp.org/mirror/pub/gimp/v2.10/windows/gimp-2.10.8-setup-2.exe) or [Photoshop](https://www.adobe.com/de/products/photoshop.html)
+ Hover your cursor over the top-left corner of the names (look at the [example.jpg](files/EXAMPLE.jpg) if you need to know what top-left corner I mean)
+ Look at the bottom of the screen, it should tell you the X and Y coordinates of your cursor, remember those and pluck them into the [config file](configs/config_example.yaml) where it says top_left
+ Now to the same thing with the bottom\_right, just hover your cursor at the bottom right of the name-list and edit the [config file](config/config_example.yaml)

## How to use
(Using the default hotkey configuration)
+ Run the [.bat script](run.bat)
+ Head into rainbow six and go into a game (casual or ranked doesn't matter)

(While in the Operator select screen)
+ Hold tab (or whatever key you set in your game configuration) to show the player-list
+ Press `CTRL+N` to take a screenshot of the game and start the lookup

(To look someone up manually)
+ Press `CTRL+M`
+ Head into the running cmd
+ Type the players name and hit enter